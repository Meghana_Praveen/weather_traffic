package com.citi.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.citi.weather.service.JavaBeans.WeatherMain;

@RestController
@RequestMapping("/weather")
public class WeatherController {
	
	@Autowired
	private RestTemplate restTemplate;
	
//	@Value("${api.key}")
//	private String apikey;
	
	private String CityName;
	
	public String getCityName() {
		return CityName;
	}

	public void setCityName(String cityName) {
		CityName = cityName;
	}


	@GetMapping("/{CityName}")
	//@CircuitBreaker(name = "weatherService", fallbackMethod = "fallBack")
	public WeatherMain getWeather(@PathVariable("CityName") String CityName) {
		WeatherMain weatherMain = new WeatherMain();
         weatherMain = restTemplate.getForObject("http://api.openweathermap.org/data/2.5/weather?q="+CityName+"&appid=4cbf132ce2c6e5cba78c588f41084bf8", WeatherMain.class);
		return weatherMain;
	}
		
	
}
