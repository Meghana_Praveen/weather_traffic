package com.citi.weather.service.JavaBeans;

public class Wind {
	 private double speed;
	    private int deg;
	    private double gust;
	    public void setSpeed(double speed) {
	         this.speed = speed;
	     }
	     public double getSpeed() {
	         return speed;
	     }

	    public void setDeg(int deg) {
	         this.deg = deg;
	     }
	     public int getDeg() {
	         return deg;
	     }

	    public void setGust(double gust) {
	         this.gust = gust;
	     }
	     public double getGust() {
	         return gust;
	     }

}
