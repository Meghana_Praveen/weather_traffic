package com.citi.traffic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/traffic")
public class TrafficController {
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping("/{lat}/{lon}")
	@CircuitBreaker(name = "trafficService", fallbackMethod = "fallBack")
	public Object gettraffic(@PathVariable(value="lat") String lat, @PathVariable(value="lon") String lon) {
        Object traffic = restTemplate.getForObject("https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/json?key=qALryA5x2ufA4ExT9f3r2zZf14bWOidB&point="+lat+","+lon, Object.class);
		return traffic;
	}
	
	public String fallBack(Exception e)
	{
		return "Traffic Service is down";
	}

}
