package com.citi.result;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.citi.result.WeatherResultsBean.WeatherMain;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


//import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/")
public class ResultController {
	
	
	@Autowired
	private RestTemplate restTemplate;
	

	@RequestMapping("/{CityName}")
	//@CircuitBreaker(name = "resultService", fallbackMethod = "fallBack")
//	public WeatherMain getWeather(@PathVariable("CityName") String CityName) {
//		WeatherMain weatherMain = new WeatherMain();
//		 weatherMain = restTemplate.getForObject("http://WEATHER-SERVICE-INFO/weather/"+CityName, WeatherMain.class);
//		
//		//Object traffic = restTemplate.getForObject("http://TRAFFIC-SERVICE-INFO/traffic/", Object.class);
//		return weatherMain;
//		}
	
	@HystrixCommand(fallbackMethod = "fallBackinfo")
	public List<Object> getWeather(@PathVariable("CityName") String CityName) {
		WeatherMain weatherMain = restTemplate.getForObject("http://WEATHER-SERVICE-INFO/weather/"+CityName, WeatherMain.class);
		System.out.println("\n\n");
		double latitude = weatherMain.getCoord().getLat();
		double longitude = weatherMain.getCoord().getLon();
		Object traffic = restTemplate.getForObject("http://TRAFFIC-SERVICE-INFO/traffic/"+latitude+"/"+longitude, Object.class);
		return Arrays.asList(weatherMain, traffic);
	}
	
	

	
	public List<Object> fallBackinfo(@PathVariable("CityName") String CityName) {
		return Arrays.asList();
	}
	
}

		
