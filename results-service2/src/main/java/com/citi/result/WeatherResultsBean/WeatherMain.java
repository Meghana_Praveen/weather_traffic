package com.citi.result.WeatherResultsBean;

import java.util.List;

public class WeatherMain {
	  private Coord coord;
	    private List<Weather> weather;
	    private Main main;
	    private int visibility;
	    private Wind wind;
	    private Clouds clouds;
	    private Sys sys;
	    private String name;
	    public void setCoord(Coord coord) {
	         this.coord = coord;
	     }
	     public Coord getCoord() {
	         return coord;
	     }

	    public void setWeather(List<Weather> weather) {
	         this.weather = weather;
	     }
	     public List<Weather> getWeather() {
	         return weather;
	     }

	    public void setMain(Main main) {
	         this.main = main;
	     }
	     public Main getMain() {
	         return main;
	     }

	    public void setVisibility(int visibility) {
	         this.visibility = visibility;
	     }
	     public int getVisibility() {
	         return visibility;
	     }

	    public void setWind(Wind wind) {
	         this.wind = wind;
	     }
	     public Wind getWind() {
	         return wind;
	     }

	    public void setClouds(Clouds clouds) {
	         this.clouds = clouds;
	     }
	     public Clouds getClouds() {
	         return clouds;
	     }

	    public void setSys(Sys sys) {
	         this.sys = sys;
	     }
	     public Sys getSys() {
	         return sys;
	     }

	    public void setName(String name) {
	         this.name = name;
	     }
	     public String getName() {
	         return name;
	     }

}
