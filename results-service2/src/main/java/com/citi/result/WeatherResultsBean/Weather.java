package com.citi.result.WeatherResultsBean;

public class Weather {
	private String main;
    private String description;
    public void setMain(String main) {
         this.main = main;
     }
     public String getMain() {
         return main;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }


}
